package com.example.domain;

import lombok.Value;

@Value
public class Entidad {

    int id;
    String nombre;

    public Entidad(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

}
