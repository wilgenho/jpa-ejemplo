package com.example.domain;

import lombok.Value;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Value
public class Envio {

    EnvioId id;
    Entidad entidad;
    int lote;
    LocalDateTime creado;
    List<EnvioEstado> estados;

    public Envio(EnvioId id, Entidad entidad, int lote, LocalDateTime creado, List<EnvioEstado> estados) {
        this.id = id;
        this.entidad = entidad;
        this.lote = lote;
        this.creado = creado;
        this.estados = estados;
    }

    public static Envio crear(EnvioId id, Entidad entidad, int lote, String estado) {
        List<EnvioEstado> estados = new ArrayList<>();
        estados.add(EnvioEstado.crear(estado));
        return new Envio(id, entidad, lote, LocalDateTime.now(), estados);
    }

    public void agregarEstado(EnvioEstadoId id, String estado, LocalDateTime actualizado) {
        estados.add(new EnvioEstado(id, estado, actualizado));
    }
}
