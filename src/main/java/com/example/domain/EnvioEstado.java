package com.example.domain;

import lombok.Value;

import java.time.LocalDateTime;

@Value
public class EnvioEstado {

    EnvioEstadoId id;
    String estado;
    LocalDateTime actualizado;

    public static EnvioEstado crear(String estado) {
        return new EnvioEstado(EnvioEstadoId.generar(), estado, LocalDateTime.now());
    }


}
