package com.example.domain;

import lombok.Value;

import java.util.UUID;

@Value
public class EnvioEstadoId {

    String value;

    public EnvioEstadoId(String value) {
        this.value = value;
    }

    public static EnvioEstadoId generar() {
        return new EnvioEstadoId(UUID.randomUUID().toString());
    }
}
