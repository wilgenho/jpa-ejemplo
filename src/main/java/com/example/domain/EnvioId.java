package com.example.domain;

import lombok.Value;

import java.util.UUID;

@Value
public class EnvioId {

    String value;

    public EnvioId(String value) {
        this.value = value;
    }

    public static EnvioId generar() {
         return new EnvioId(UUID.randomUUID().toString());
    }
}
