package com.example.domain;

public interface EnvioRepository {

    void guardar(Envio envio);
    Envio consultar(String id);
    Envio buscarLote(int entidad, int lote);

}
