package com.example.infra;

import com.example.domain.Entidad;
import com.example.domain.Envio;
import com.example.domain.EnvioEstado;
import com.example.domain.EnvioId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "ENVIOS")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EnvioEntity {

    @Id @Column(length = 36)
    private String id;
    private int entidad;
    private String nombre;
    private int lote;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    @JoinColumn(name = "envio_id")
    private List<EnvioEstadoEntity> estados;

    public static EnvioEntity fromDomain(Envio envio) {
        List<EnvioEstadoEntity> listaEstados = envio.getEstados().stream().map(EnvioEstadoEntity::fromDomain).collect(Collectors.toList());
        return new EnvioEntity(envio.getId().getValue(), envio.getEntidad().getId(), envio.getEntidad().getNombre(), envio.getLote(), listaEstados);
    }

    public Envio toDomain() {
        List<EnvioEstado> listaEstados = estados.stream().map(EnvioEstadoEntity::toDomain).collect(Collectors.toList());
        return new Envio(new EnvioId(id), new Entidad(entidad, nombre), lote, LocalDateTime.now(), listaEstados);
    }

}
