package com.example.infra;

import com.example.domain.EnvioEstado;
import com.example.domain.EnvioEstadoId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "envios_estados")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EnvioEstadoEntity {

    @Id
    @Column(length = 36)
    private String id;

    @Column(length = 20)
    private String estado;

    private LocalDateTime actualizado;

    public static EnvioEstadoEntity fromDomain(EnvioEstado envioEstados) {
        return new EnvioEstadoEntity(envioEstados.getId().getValue(), envioEstados.getEstado(), envioEstados.getActualizado());
    }

    public EnvioEstado toDomain() {
        return new EnvioEstado(new EnvioEstadoId(id), estado, actualizado);
    }
}
