package com.example.infra;

import com.example.domain.Envio;
import com.example.domain.EnvioRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@SuppressWarnings("unused")
public class EnvioRepositoryImpl implements EnvioRepository {

    private final EnvioRepositoryJPA envioRepositoryJPA;

    public EnvioRepositoryImpl(final EnvioRepositoryJPA envioRepositoryJPA) {
        this.envioRepositoryJPA = envioRepositoryJPA;
    }

    @Override
    public void guardar(Envio envio) {
        envioRepositoryJPA.save(EnvioEntity.fromDomain(envio));
    }

    @Override
    public Envio consultar(String id) {
        return envioRepositoryJPA.findById(id).map(EnvioEntity::toDomain).orElse(null);
    }

    @Override
    public Envio buscarLote(int entidad, int lote) {
        return envioRepositoryJPA.findFirstByEntidadAndLote(entidad, lote).map(EnvioEntity::toDomain).orElse(null);
    }
}

interface EnvioRepositoryJPA extends JpaRepository<EnvioEntity, String> {
    Optional<EnvioEntity> findFirstByEntidadAndLote(int entidad, int lote);
}
