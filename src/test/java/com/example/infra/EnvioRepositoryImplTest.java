package com.example.infra;

import com.example.domain.*;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;


@SpringBootTest
@Log4j2
class EnvioRepositoryImplTest {

    @Autowired
    EnvioRepository repository;

    @Test
    void deberiaGuardarEnvio() {

        Envio ejemplo = ejemploVisa();
        log.info("\n==============================>  Grabo el envio {} " , ejemplo.getId());
        repository.guardar(ejemplo);

        log.info("\n==============================> Registro grabado ");

        Envio buscado = repository.consultar(ejemplo.getId().getValue());
        log.info("\n==============================>  Consulto el envio {} " , buscado);
        assertEquals("VISA", buscado.getEntidad().getNombre());

        log.info("\n==============================>  Actualizar estado ");
        buscado.agregarEstado(EnvioEstadoId.generar(), "PRUEBA", LocalDateTime.now());
        repository.guardar(buscado);
    }

    @Test
    void deberiaEncontrarEnvio() {
        repository.guardar(ejemploAmex());
        log.info("\n==============================>  Buscado un envio por entidad y lote ");
        Envio buscado = repository.buscarLote(5, 189);
        assertEquals(5, buscado.getEntidad().getId());
    }

    @Test
    void noDeberiaEncontrarEnvio() {
        log.info("\n==============================>  Buscado un envio que no existe ");
        Envio buscado = repository.buscarLote(18, 457);
        assertNull(buscado);
    }

    private Envio ejemploVisa() {
        return Envio.crear(EnvioId.generar(), new Entidad(1, "VISA"), 189, "ENVIADO");
    }

    private Envio ejemploAmex() {
        return Envio.crear(EnvioId.generar(), new Entidad(5, "AMEX"), 189, "ENVIADO");
    }

}